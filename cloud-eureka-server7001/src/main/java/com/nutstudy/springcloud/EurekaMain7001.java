package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 16:44
 * @description:
 */

@EnableEurekaServer
@SpringBootApplication
public class EurekaMain7001
{
    public static void main(String[] args)
    {
        SpringApplication.run(EurekaMain7001.class, args);
    }
}
