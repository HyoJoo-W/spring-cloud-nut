package com.nutstudy.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 14:22
 * @description: vo实体
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T>
{
    private Integer code;//响应状态码
    private String message;//响应消息
    private T data;//响应体

    public CommonResult(Integer code, String message)
    {
        this(code, message, null);
    }

}
