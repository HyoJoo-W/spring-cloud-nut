package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 16:13
 * @description:
 */

@SpringBootApplication
public class PaymentHystrixMain8000
{
    public static void main(String[] args)
    {
        SpringApplication.run(PaymentHystrixMain8000.class, args);
    }
}
