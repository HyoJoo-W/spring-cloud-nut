package com.nutstudy.springcloud.controller;

import com.nutstudy.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 16:19
 * @description:
 */

@Slf4j
@RestController
public class PaymentController
{
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/payment/hystrix/ok/{id}")
    public String payment_OK(@PathVariable("id") Integer id)
    {
        String ok = paymentService.paymentInfo_OK(id);
        log.info("result: " + ok);
        return ok;
    }

    @GetMapping("/payment/hystrix/timeout/{id}")
    public String payment_Timeout(@PathVariable("id") Integer id)
    {
        String timeout = paymentService.paymentInfo_Timeout(id);
        log.info("result: " + timeout);
        return timeout;
    }

}
