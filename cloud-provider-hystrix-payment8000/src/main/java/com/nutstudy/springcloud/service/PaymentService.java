package com.nutstudy.springcloud.service;

import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 16:15
 * @description:
 */

@Service
public class PaymentService
{
    public String paymentInfo_OK(Integer id)
    {
        return "线程池:" + Thread.currentThread().getName() + " paymentInfo_OK, id: " + id;
    }

    public String paymentInfo_Timeout(Integer id)
    {
        try
        {
            TimeUnit.SECONDS.sleep(3);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        return "线程池:" + Thread.currentThread().getName() + " paymentInfo_Timeout, id: " + id;
    }
}
