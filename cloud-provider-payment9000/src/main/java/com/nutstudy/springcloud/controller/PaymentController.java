package com.nutstudy.springcloud.controller;

import com.nutstudy.springcloud.entity.CommonResult;
import com.nutstudy.springcloud.entity.Payment;
import com.nutstudy.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 14:44
 * @description:
 */

@Slf4j
@RestController
public class PaymentController
{
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment)
    {
        int result = paymentService.create(payment);
        log.info("插入结果:" + result);

        if (result > 0)
            return new CommonResult(200, "插入数据库成功,server.port = " + serverPort, result);
        else
            return new CommonResult(500, "插入数据库失败", result);
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id)
    {
        Payment result = paymentService.getPaymentById(id);
        log.info("查询结果:(查询成功可以看到这里...)" + result);

        if (result != null)
            return new CommonResult(200, "查询数据库成功,server.port = " + serverPort, result);
        else
            return new CommonResult(500, "查询数据库失败,查询ID: " + id, null);
    }

    @GetMapping("/payment/lb")
    public String getPaymentLB()
    {
        return serverPort;
    }
}
