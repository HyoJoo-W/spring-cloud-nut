package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 17:31
 * @description:
 */

@EnableEurekaClient
@SpringBootApplication
public class PaymentMain9000
{
    public static void main(String[] args)
    {
        SpringApplication.run(PaymentMain9000.class, args);
    }
}
