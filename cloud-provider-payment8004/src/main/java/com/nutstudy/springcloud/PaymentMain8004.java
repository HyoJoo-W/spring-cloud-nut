package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 09:41
 * @description:
 */

@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain8004
{
    public static void main(String[] args)
    {
        SpringApplication.run(PaymentMain8004.class, args);
    }
}
