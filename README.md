
# 一、SpringCloud脑图
![img.png](assert/image/springcloud.png)

# 二、仓库说明
## （一）master分支
- 该分支是SpringCloud一般性的组件练习

## （二）netflix分支
- 该分支是SpringCloud-Netflix系列解决方案
### 微服务架构的核心问题
- 服务发现
- 服务通信
- 服务治理
- 服务异常
### 项目给出的方案
- RestTemplate（服务访问）
- Eureka（服务发现）
- Ribbon、Feign（负载均衡）
- Hystrix（熔断降级）

## （三）alibaba分支
- 该分支是SpringCloud-Alibaba系列解决方案
    ![img_1.png](assert/image/spirngcloudalibaba.png)
# 三、参与贡献
- @HyoJoo-W