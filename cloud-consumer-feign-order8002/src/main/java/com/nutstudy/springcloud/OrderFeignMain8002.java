package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 14:25
 * @description:
 */

@EnableFeignClients
@SpringBootApplication
public class OrderFeignMain8002
{
    public static void main(String[] args)
    {
        SpringApplication.run(OrderFeignMain8002.class, args);
    }
}
