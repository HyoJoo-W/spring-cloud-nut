package com.nutstudy.springcloud.service;

import com.nutstudy.springcloud.entity.CommonResult;
import com.nutstudy.springcloud.entity.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 14:26
 * @description:
 */

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService
{
    //注解上面写的是服务提供者的请求路径
    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);
}
