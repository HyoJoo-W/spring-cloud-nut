package com.nutstudy.springcloud.controller;

import com.nutstudy.springcloud.entity.CommonResult;
import com.nutstudy.springcloud.entity.Payment;
import com.nutstudy.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 14:30
 * @description:
 */

@Slf4j
@RestController
public class OrderFeignController
{
    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id)
    {
        return paymentFeignService.getPaymentById(id);
    }
}
