package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 10:12
 * @description:
 */

@EnableDiscoveryClient
@SpringBootApplication
public class OrderZKMain8002
{
    public static void main(String[] args)
    {
        SpringApplication.run(OrderZKMain8002.class, args);
    }
}
