package com.nutstudy.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 13:07
 * @description:
 */

@Configuration
public class NutRule
{
    @Bean
    public IRule myRule()
    {
        return new RandomRule();
    }
}
