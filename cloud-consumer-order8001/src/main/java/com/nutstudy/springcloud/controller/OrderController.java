package com.nutstudy.springcloud.controller;

import com.nutstudy.springcloud.entity.CommonResult;
import com.nutstudy.springcloud.entity.Payment;
import com.nutstudy.springcloud.lb.LoadBalancer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 15:24
 * @description:
 */

@Slf4j
@RestController
public class OrderController
{
    private static final String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private LoadBalancer loadBalancer;

    @Resource
    private DiscoveryClient discoveryClient;

    @GetMapping("/consumer/payment/create")
    public CommonResult<Payment> create(Payment payment)
    {
        return restTemplate.postForObject(PAYMENT_URL + "/payment/create",
                                          payment,
                                          CommonResult.class);
    }

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id)
    {
        return restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id,
                                         CommonResult.class);
    }


    @GetMapping("/consumer/payment/getForEntity/{id}")
    public CommonResult<Payment> getPayment(@PathVariable("id") Long id)
    {
        ResponseEntity<CommonResult> entity = restTemplate.getForEntity(PAYMENT_URL + "/payment/get/" + id,
                                                                        CommonResult.class);
        if (entity.getStatusCode().is2xxSuccessful())
        {
            return entity.getBody();
        } else
        {
            return new CommonResult<>(500, "操作失败");
        }
    }

    @GetMapping("/consumer/payment/lb")
    public String getLb()
    {
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if (instances == null || instances.size() <= 0)
        {
            return null;
        }
        ServiceInstance instances1 = loadBalancer.instances(instances);
        URI uri = instances1.getUri();
        log.info("uri :" + uri);

        return restTemplate.getForObject(uri + "/payment/lb", String.class);
    }


    @GetMapping("/consumer/payment/zipkin")
    public String paymentZipkin()
    {
        return restTemplate.getForObject("http://localhost:8000/payment/zipkin", String.class);
    }
}
