package com.nutstudy.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 13:32
 * @description:
 */


public interface LoadBalancer
{
    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
