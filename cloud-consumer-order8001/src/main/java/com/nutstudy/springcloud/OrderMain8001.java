package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 15:21
 * @description:
 */

//@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = NutRule.class)s
@EnableEurekaClient
@SpringBootApplication
public class OrderMain8001
{
    public static void main(String[] args)
    {
        SpringApplication.run(OrderMain8001.class, args);
    }
}
