package com.nutstudy.springcloud.dao;

import com.nutstudy.springcloud.entity.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 14:25
 * @description:
 */

@Mapper
public interface PaymentDao
{
    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);
}
