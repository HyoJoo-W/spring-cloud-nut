package com.nutstudy.springcloud.service.impl;

import com.nutstudy.springcloud.dao.PaymentDao;
import com.nutstudy.springcloud.entity.Payment;
import com.nutstudy.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 14:42
 * @description:
 */

@Service
public class PaymentServiceImpl implements PaymentService
{
    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment)
    {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id)
    {
        return paymentDao.getPaymentById(id);
    }
}
