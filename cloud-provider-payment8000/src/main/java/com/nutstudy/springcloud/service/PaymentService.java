package com.nutstudy.springcloud.service;

import com.nutstudy.springcloud.entity.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 14:41
 * @description:
 */


public interface PaymentService
{
    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);
}
