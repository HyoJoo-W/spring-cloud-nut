package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-30 14:09
 * @description:
 */

@EnableDiscoveryClient
@EnableEurekaClient
@SpringBootApplication
public class PaymentMain8000
{
    public static void main(String[] args)
    {
        SpringApplication.run(PaymentMain8000.class, args);
    }
}
