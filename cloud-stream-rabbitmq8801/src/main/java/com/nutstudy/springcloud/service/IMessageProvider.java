package com.nutstudy.springcloud.service;

/**
 * @author: HyoJoo-W
 * @date: 2021-08-01 10:25
 * @description:
 */


public interface IMessageProvider
{
    public String send();
}
