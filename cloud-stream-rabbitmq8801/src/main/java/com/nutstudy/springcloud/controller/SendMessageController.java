package com.nutstudy.springcloud.controller;

import com.nutstudy.springcloud.service.IMessageProvider;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: HyoJoo-W
 * @date: 2021-08-01 10:36
 * @description:
 */

@RestController
public class SendMessageController
{
    @Resource
    private IMessageProvider messageProvider;

    @GetMapping("sendMessage")
    public String sendMessage()
    {
        return messageProvider.send();
    }
}
