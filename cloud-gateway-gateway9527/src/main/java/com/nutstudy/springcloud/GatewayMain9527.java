package com.nutstudy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: HyoJoo-W
 * @date: 2021-07-31 20:43
 * @description:
 */

@EnableEurekaClient
@SpringBootApplication
public class GatewayMain9527
{
    public static void main(String[] args)
    {
        SpringApplication.run(GatewayMain9527.class, args);
    }
}
